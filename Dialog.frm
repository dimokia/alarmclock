VERSION 5.00
Begin VB.Form Dialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "���������!"
   ClientHeight    =   1515
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   5775
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1515
   ScaleWidth      =   5775
   ShowInTaskbar   =   0   'False
   Begin VB.VScrollBar VScroll2 
      Height          =   495
      Left            =   2880
      Max             =   0
      Min             =   -59
      TabIndex        =   6
      Top             =   600
      Width           =   255
   End
   Begin VB.VScrollBar VScroll1 
      Height          =   495
      Left            =   960
      Max             =   0
      Min             =   -23
      TabIndex        =   5
      Top             =   600
      Width           =   255
   End
   Begin VB.TextBox Text5 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   15.75
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2280
      TabIndex        =   3
      Text            =   "Text2"
      Top             =   600
      Width           =   615
   End
   Begin VB.TextBox Text4 
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   15.75
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   600
      Width           =   615
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "������"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4440
      TabIndex        =   1
      Top             =   840
      Width           =   1215
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   4440
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label2 
      Alignment       =   2  '���������
      Caption         =   "�����"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3120
      TabIndex        =   8
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   2  '���������
      Caption         =   "�����"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1200
      TabIndex        =   7
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label6 
      Alignment       =   2  '���������
      Caption         =   "��������� ��������� �����"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   4215
   End
End
Attribute VB_Name = "Dialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CancelButton_Click()
    Form1.Frame2.Visible = False
    Form1.Timer2.Enabled = False
    Unload Dialog
End Sub

Private Sub OKButton_Click()
    If Len(Text4.Text) < 2 Then Text4.Text = 0 & Text4.Text
    If Len(Text5.Text) < 2 Then Text5.Text = 0 & Text5.Text
    Form1.Frame1.Visible = False
    Form1.Frame2.Visible = True
    Form1.Timer2.Enabled = True
    Dialog.Hide
    Form1.Label8.Caption = Text4.Text
    Form1.Label9.Caption = Text5.Text
    
    
    
End Sub


Private Sub Text4_Change()
    On Error Resume Next
    If Text4.Text = "" Then Text4.Text = "00"
    If Text4.Text > 23 Then Text4.Text = 23
End Sub

Private Sub Text5_Change()
    On Error Resume Next
    If Text5.Text = "" Then Text5.Text = "00"
    If Text5.Text > 59 Then Text5.Text = 59
End Sub


Private Sub VScroll1_Change()
    Dim newValue As Long
    newValue = -VScroll1.Value
    If newValue < 10 Then
        Text4.Text = "0" & newValue
    Else
        Text4.Text = newValue
    End If
End Sub

Private Sub VScroll2_Change()
    Dim newValue As Long
    newValue = -VScroll2.Value
    If newValue < 10 Then
        Text5.Text = "0" & newValue
    Else
        Text5.Text = newValue
    End If
End Sub

